#include "mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("Terrain Renderer");

    m_timingGraph = new TimingGraph();
    QChartView *chartView = new QChartView(m_timingGraph->getChart());
    chartView->setRenderHint(QPainter::Antialiasing);
    QDockWidget *dockWidget = new QDockWidget("Timing Graph");
    dockWidget->setWidget(chartView);
    addDockWidget(Qt::DockWidgetArea::BottomDockWidgetArea, dockWidget);

    m_glWidget = new HelloGLWidget(m_timingGraph);
    setCentralWidget(m_glWidget);

    QMenuBar *menuBar = new QMenuBar();

    QMenu *fileMenu = menuBar->addMenu("&File");
    QAction *openAction = fileMenu->addAction("&Open");
    QAction *loadAction = fileMenu->addAction("Load Tree");
    fileMenu->addSeparator();
    QAction *exitAction = fileMenu->addAction("E&xit");
    exitAction->setShortcut(QKeySequence("Ctrl+q"));

    QMenu *helpMenu = menuBar->addMenu("Help");
    QAction *aboutAction = helpMenu->addAction("About");

    setMenuBar(menuBar);

    QToolBar *toolBar = new QToolBar();
    QAction *wireframeAction = toolBar->addAction("Wireframe");
    wireframeAction->setCheckable(true);
    wireframeAction->setIcon(QIcon(":/img/wireframe.png"));
    addToolBar(toolBar);

    toolBar = new QToolBar();
    QLabel *label = new QLabel("X: ");
    toolBar->addWidget(label);

    QSpinBox *lpX = new QSpinBox();
    lpX->setRange(-1000, 1000);
    lpX->setValue(WORLD_SIZE / 2);
    toolBar->addWidget(lpX);

    label = new QLabel(" Y: ");
    toolBar->addWidget(label);

    QSpinBox *lpY = new QSpinBox();
    lpY->setRange(10, 1000);
    lpY->setValue(200);
    toolBar->addWidget(lpY);

    label = new QLabel(" Z: ");
    toolBar->addWidget(label);

    QSpinBox *lpZ = new QSpinBox();
    lpZ->setRange(-1000, 1000);
    lpZ->setValue(-WORLD_SIZE / 2);
    toolBar->addWidget(lpZ);

    label = new QLabel(" I: ");
    toolBar->addWidget(label);

    QSpinBox *lI = new QSpinBox();
    lI->setRange(0, 1000);
    lI->setValue(1);
    toolBar->addWidget(lI);

    label = new QLabel(" R: ");
    toolBar->addWidget(label);

    QSpinBox *lR = new QSpinBox();
    lR->setRange(0, 255);
    lR->setValue(255);
    toolBar->addWidget(lR);

    label = new QLabel(" G: ");
    toolBar->addWidget(label);

    QSpinBox *lG = new QSpinBox();
    lG->setRange(0, 255);
    lG->setValue(255);
    toolBar->addWidget(lG);

    label = new QLabel(" B: ");
    toolBar->addWidget(label);

    QSpinBox *lB = new QSpinBox();
    lB->setRange(0, 255);
    lB->setValue(255);
    toolBar->addWidget(lB);

    addToolBar(toolBar);

    toolBar = new QToolBar();

    QLabel *verticalScalingLabel = new QLabel("V: ");
    toolBar->addWidget(verticalScalingLabel);
    QSlider *verticalScalingSlider = new QSlider();
    verticalScalingSlider->setRange(1, 50);
    verticalScalingSlider->setSingleStep(1);
    verticalScalingSlider->setOrientation(Qt::Horizontal);
    toolBar->addWidget(verticalScalingSlider);

    QLabel *horizontalScalingLabel = new QLabel("  H: ");
    toolBar->addWidget(horizontalScalingLabel);
    QSlider *horizontalScalingSlider = new QSlider();
    horizontalScalingSlider->setRange(1, 50);
    horizontalScalingSlider->setSingleStep(1);
    horizontalScalingSlider->setOrientation(Qt::Horizontal);
    toolBar->addWidget(horizontalScalingSlider);

    addToolBar(toolBar);
    addToolBarBreak(Qt::ToolBarArea::TopToolBarArea);

    toolBar = new QToolBar();

    QLabel *numTreesLabel = new QLabel("Trees: ");
    toolBar->addWidget(numTreesLabel);
    QSpinBox *numTrees = new QSpinBox();
    numTrees->setRange(10, 100);
    numTrees->setValue(100);
    toolBar->addWidget(numTrees);

    QLabel *geomDistLabel = new QLabel(" G-Dist: ");
    toolBar->addWidget(geomDistLabel);
    QSlider *geomDist = new QSlider();
    geomDist->setRange(10, 30);
    geomDist->setSingleStep(1);
    geomDist->setValue(20);
    geomDist->setOrientation(Qt::Horizontal);
    toolBar->addWidget(geomDist);

    QLabel *impDistLabel = new QLabel(" I-Dist: ");
    toolBar->addWidget(impDistLabel);
    QSlider *impDist = new QSlider();
    impDist->setRange(20, 50);
    impDist->setSingleStep(1);
    impDist->setValue(40);
    impDist->setOrientation(Qt::Horizontal);
    toolBar->addWidget(impDist);

    QLabel *rotsLabel = new QLabel(" Rot Angles: ");
    toolBar->addWidget(rotsLabel);
    QSlider *rots = new QSlider();
    rots->setRange(10, 36);
    rots->setSingleStep(1);
    rots->setValue(36);
    rots->setOrientation(Qt::Horizontal);
    toolBar->addWidget(rots);

    addToolBar(toolBar);

    connect(openAction, &QAction::triggered, this, &MainWindow::openFile);
    connect(loadAction, &QAction::triggered, this, &MainWindow::loadTree);
    connect(exitAction, &QAction::triggered, this, &MainWindow::close);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::showAboutBox);
    connect(wireframeAction, &QAction::triggered, m_glWidget, &HelloGLWidget::toggleWireframe);
    connect(lpX, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightX);
    connect(lpY, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightY);
    connect(lpZ, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightZ);
    connect(lI, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightIntensity);
    connect(lR, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightR);
    connect(lG, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightG);
    connect(lB, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setLightB);
    connect(verticalScalingSlider, &QSlider::valueChanged, m_glWidget, &HelloGLWidget::updateVerticalScaling);
    connect(horizontalScalingSlider, &QSlider::valueChanged, m_glWidget, &HelloGLWidget::updateHorizontalScaling);
    connect(numTrees, QOverload<int>::of(&QSpinBox::valueChanged), m_glWidget, &HelloGLWidget::setNumberOfTrees);
    connect(geomDist, &QSlider::valueChanged, m_glWidget, &HelloGLWidget::setGeomDist);
    connect(impDist, &QSlider::valueChanged, m_glWidget, &HelloGLWidget::setImpDist);
    connect(rots, &QSlider::valueChanged, m_glWidget, &HelloGLWidget::setRots);

    resize(1280, 720);
}

void MainWindow::openFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open Height Map", QDir::currentPath(), "Height Maps (*.pgm)");
    if (fileName.isNull())
        return;

    HeightMap* heightMap = new HeightMap(fileName);
    m_glWidget->setHeightMap(heightMap);
}

void MainWindow::loadTree()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open Tree", QDir::currentPath(), "Trees (*.gltf)");
    if (fileName.isNull())
        return;

    m_glWidget->loadTree(fileName);
}

void MainWindow::showAboutBox()
{
    std::string text = "Written by Simon Grossmann\n\nOpen GL Version: ";

    QSurfaceFormat format = m_glWidget->format();

    char versionBuffer[100];
    snprintf(versionBuffer, sizeof(versionBuffer), "%d.%d", format.majorVersion(), format.minorVersion());
    text.append(versionBuffer);

    text.append("\nOpenGL Profile: ");
    std::string profileNames[] = {"No Profile", "Core Profile", "Compatibility Profile"};
    text.append(profileNames[format.profile()]);

    QMessageBox::information(this, "About Hello GL", text.c_str());
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    m_glWidget->keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    m_glWidget->keyReleaseEvent(event);
}
