#include "camera.h"
#include <iostream>

Camera::Camera(QVector3D startLookAt)
    : m_defaultPosition(startLookAt + QVector3D(2, 0.5F, 3)),
      m_position(startLookAt + QVector3D(2, 0.5F, 3)),
      m_lookAt(startLookAt)
{

}

QVector2D Camera::getOffset() const
{
    float tx = m_position.x() - m_defaultPosition.x();
    float tz = m_position.z() - m_defaultPosition.z();
    return QVector2D(-tx, tz);
}

QMatrix4x4 Camera::getModelMatrix() const
{
    QMatrix4x4 m;
    float tx = m_position.x() - m_defaultPosition.x();
    float tz = m_position.z() - m_defaultPosition.z();
    m.translate(tx, 0, tz);
    return m;
}

QMatrix4x4 Camera::getViewMatrix() const
{
    QMatrix4x4 v;
    v.lookAt(m_position, m_lookAt, m_up);
    return v;
}

QVector3D Camera::getPosition() const
{
    return m_position;
}

std::vector<QVector4D> Camera::getViewFrustum(QMatrix4x4 &p)
{
    QMatrix4x4 mat = (p * getViewMatrix()).inverted();

    std::vector<QVector4D> frustum;
    frustum.push_back(createFrustum(mat, QVector3D(1, -1, -1), QVector3D(1, 1, -1), QVector3D(1, -1, 1))); // right
    frustum.push_back(createFrustum(mat, QVector3D(-1, 1, -1), QVector3D(-1, -1, -1), QVector3D(-1, 1, 1))); // left
    frustum.push_back(createFrustum(mat, QVector3D(1, 1, -1), QVector3D(-1, 1, -1), QVector3D(1, 1, 1))); // top
    frustum.push_back(createFrustum(mat, QVector3D(-1, -1, -1), QVector3D(1, -1, -1), QVector3D(-1, -1, 1))); // bottom
    frustum.push_back(createFrustum(mat, QVector3D(-1, -1, -1), QVector3D(-1, 1, -1), QVector3D(1, -1, -1))); // near
    frustum.push_back(createFrustum(mat, QVector3D(-1, 1, 1), QVector3D(-1, -1, -1), QVector3D(1, 1, 1))); // far
    return frustum;
}

QVector3D Camera::moveLight(QVector3D pos)
{
    QVector2D offset = getOffset();
    pos.setX(pos.x() - offset.x());
    pos.setZ(pos.z() + offset.y());
    return pos;
}

void Camera::move(bool w, bool a, bool s, bool d)
{
    QVector3D direction = (m_lookAt - m_position);
    direction.setY(direction.y() * 0.01F);
    direction.normalize();
    QVector3D right = QVector3D::crossProduct(direction, m_up).normalized();

    if (w)
    {
        m_position += direction * m_moveSpeed;
        m_lookAt += direction * m_moveSpeed;
    }

    if (a)
    {
        m_position -= right * m_moveSpeed;
        m_lookAt -= right * m_moveSpeed;
    }

    if (s)
    {
        m_position -= direction * m_moveSpeed;
        m_lookAt -= direction * m_moveSpeed;
    }

    if (d)
    {
        m_position += right * m_moveSpeed;
        m_lookAt += right * m_moveSpeed;
    }
}

void Camera::rotate(QVector2D from, QVector2D to)
{
    QVector2D delta = to - from;
    QMatrix4x4 rotation;
    rotation.rotate(-delta.x() * m_mouseSpeed * 50, 0, 1);
    rotation.rotate(-delta.y() * m_mouseSpeed * 50, 1, 0);
    m_position = rotation * (m_position - m_lookAt) + m_lookAt;
}

QVector4D Camera::createFrustum(QMatrix4x4 mat, QVector3D p0, QVector3D p1, QVector3D p2)
{
    QVector3D p = mat * p0;
    QVector3D n = QVector3D::crossProduct(mat * p1 - p, mat * p2 - p).normalized();
    return QVector4D(n, -QVector3D::dotProduct(p, n));
}
