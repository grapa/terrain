#include "heightmap.h"
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <iostream>

HeightMap::HeightMap(QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);
    QString type = in.readLine();
    m_width = in.readLine().toInt();
    m_height = in.readLine().toInt();
    m_max = in.readLine().toUShort();
    m_max = 0;

    file.reset();
    file.skip(in.pos() + 1);

    MipMap map;
    map.width = m_width;
    map.height = m_height;
    map.data = new std::vector<quint16>();

    QDataStream data(&file);
    unsigned char* buffer = new unsigned char[2];
    for (int i = 0; i < m_height; i++)
    {
        for (int j = 0; j < m_width; j++)
        {
            data.readRawData(reinterpret_cast<char*>(buffer), 2);
            quint16 byte0 = static_cast<quint16>(buffer[0]);
            quint16 byte1 = static_cast<quint16>(buffer[1]);

            quint16 value = static_cast<quint16>((byte0 << 8) | byte1);
            map.data->push_back(value);
            if (value > m_max)
            {
                m_max = value;
            }
        }
    }
    delete [] buffer;
    m_maps.push_back(map);

    int width = m_width;
    int height = m_height;
    while (width > 1 || height > 1)
    {
        MipMap mipmap;
        mipmap.width = width / 2;
        mipmap.height = height / 2;
        mipmap.data = new std::vector<quint16>();

        MipMap &src = m_maps[m_maps.size() - 1];
        for (int i = 0; i < height / 2; i++)
        {
            for (int j = 0; j < width / 2; j++)
            {
                int y = i * 2;
                int x = j * 2;

                quint16 value = src.data->at(y * width + x);
                value += src.data->at(y * width + x + 1);
                value += src.data->at((y + 1) * width + x);
                value += src.data->at((y + 1) * width + x + 1);

                mipmap.data->push_back(value / 4);
            }
        }

        m_maps.push_back(mipmap);
        width /= 2;
        height /= 2;
    }
}

int HeightMap::width()
{
    return m_width;
}

int HeightMap::height()
{
    return m_height;
}

quint16 HeightMap::max()
{
    return m_max;
}

std::vector<MipMap>* HeightMap::mipmaps()
{
    return &m_maps;
}
