#version 440

struct Material
{
    vec4 diffuseFactor;
    int diffuseTexture;
    float shininessFactor;
    int shininessTexture;
    int specularTexture;
    vec3 specularFactor;
};

in vec3 pos_FS;
in vec3 normal_FS;
in vec2 uv_FS;

out vec4 frag;

uniform int materialId;
uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform float lightIntensity;
uniform vec3 lightColor;

layout(binding = 3) uniform sampler2DArray textureArray;

layout(std140, binding = 4) uniform MaterialBlock
{
    Material materials[256];
};

void main(void)
{
    Material material = materials[materialId];
    vec3 k_d = clamp(vec3(material.diffuseFactor), vec3(0), vec3(1));
    vec3 k_s = clamp(material.specularFactor, vec3(0), vec3(1));
    float n = max(material.shininessFactor, 0);

    if (material.diffuseTexture >= 0)
    {
        vec4 texVal = texture(textureArray, vec3(uv_FS, material.diffuseTexture));
        if (texVal.a == 0)
            discard;

        k_d *= vec3(texVal);
    }

    if (material.specularTexture >= 0)
    {
        k_s *= vec3(texture(textureArray, vec3(uv_FS, material.specularTexture)));
    }

    if (material.shininessTexture >= 0)
    {
        n *= length(texture(textureArray, vec3(uv_FS, material.shininessTexture)));
    }

    vec3 k_a = 0.1 * k_d;
    float I_L = lightIntensity;
    vec3 V = normalize(pos_FS - cameraPos);
    vec3 N = normalize(normal_FS);
    vec3 L = normalize(lightPos - pos_FS);
    vec3 R = normalize(reflect(L, N));

    vec3 color = lightColor * (k_a * I_L + k_d * I_L * max(0, dot(N, L)) + k_s * I_L * pow(max(0, dot(R, -V)), n));
    frag = vec4(color, 1);
}
