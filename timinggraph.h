#ifndef TIMINGGRAPH_H
#define TIMINGGRAPH_H

#include <QWidget>
#include <QtCharts>

class TimingGraph
{
public:
    TimingGraph();

    QChart* getChart();
    void pushData(int frame, std::vector<uint64_t> &data);

private:
    QChart* m_chart;
    QValueAxis* m_xAxis;
    QValueAxis* m_yAxis;
    std::vector<QLineSeries*> m_series;
};

#endif // TIMINGGRAPH_H
