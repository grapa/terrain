#include "worldgenerator.h"
#include <iostream>

WorldGenerator::WorldGenerator()
{

}

World WorldGenerator::generateWorld(unsigned int size)
{
    World world;

    unsigned int verticesPerRow = size + 1;
    world.numVertices = verticesPerRow * verticesPerRow;

    unsigned int patchesPerRow = size;
    world.numPatches = patchesPerRow * patchesPerRow;

    world.vertices = new GLfloat[3 * world.numVertices];
    for (unsigned int z = 0; z < verticesPerRow; z++)
    {
        for (unsigned int x = 0; x < verticesPerRow; x++)
        {
            unsigned int offset = 3 * (z * verticesPerRow + x);
            world.vertices[offset + 0] = x;
            world.vertices[offset + 1] = 0;
            world.vertices[offset + 2] = -static_cast<int>(z);
        }
    }

    world.textureCoords = new GLfloat[2 * world.numVertices];
    for (unsigned int z = 0; z < verticesPerRow; z++)
    {
        for (unsigned int x = 0; x < verticesPerRow; x++)
        {
            unsigned int offset = 2 * (z * verticesPerRow + x);
            world.textureCoords[offset + 0] = float(x) / size;
            world.textureCoords[offset + 1] = float(z) / size;
        }
    }

    world.indices = new GLuint[4 * world.numPatches];
    for (unsigned int z = 0; z < patchesPerRow; z++)
    {
        for (unsigned int x = 0; x < patchesPerRow; x++)
        {
            unsigned int offset = 4 * (z * patchesPerRow + x);
            world.indices[offset + 0] = z * verticesPerRow + x;
            world.indices[offset + 1] = z * verticesPerRow + x + 1;
            world.indices[offset + 2] = (z + 1) * verticesPerRow + x;
            world.indices[offset + 3] = (z + 1) * verticesPerRow + x + 1;
        }
    }

    return world;
}
