#version 440

#define PI 3.14159

layout(location = 0) in vec3 pos_VS;
layout(location = 1) in vec3 normal_VS;
layout(location = 2) in vec2 uv_VS;
layout(location = 3) in vec4 offset_VS;

out vec3 pos_FS;
out vec3 normal_FS;
out vec2 uv_FS;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;
uniform mat4 n;
uniform vec2 cameraOffset;
uniform vec2 worldSize;
uniform vec2 heightMapSize;
uniform float verticalScale;
uniform float horizontalScale;

layout(binding = 0) uniform isampler2D heightMap;

mat4 rotMat(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1 - c;

    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
                oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
                oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
                0.0, 0.0, 0.0, 1.0);
}

void main(void)
{
    vec3 pos = pos_VS * offset_VS.z;

    pos = vec3(rotMat(vec3(1, 0, 0), PI / 2) * vec4(pos, 1));
    pos = vec3(rotMat(vec3(0, 1, 0), offset_VS.w) * vec4(pos, 1));

    pos.x += offset_VS.x;
    pos.z += offset_VS.y;

    vec2 uv = pos.xz * (horizontalScale / heightMapSize) - cameraOffset / heightMapSize;
    uv.y *= -1;

    pos.y += texture(heightMap, uv).r * verticalScale;

    gl_Position = (p * v * m) * vec4(pos, 1);
    pos_FS = vec3(m * vec4(pos, 1));
    normal_FS = normalize(mat3(n) * normal_VS);
    uv_FS = uv_VS;
}
