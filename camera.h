#ifndef CAMERA_H
#define CAMERA_H

#include <QVector2D>
#include <QMatrix4x4>
#include <QQuaternion>

class Camera
{
public:
    Camera(QVector3D startLookAt);

    QVector2D getOffset() const;
    QMatrix4x4 getModelMatrix() const;
    QMatrix4x4 getViewMatrix() const;
    QVector3D getPosition() const;
    std::vector<QVector4D> getViewFrustum(QMatrix4x4 &p);

    QVector3D moveLight(QVector3D pos);
    void move(bool w, bool a, bool s, bool d);
    void rotate(QVector2D from, QVector2D to);

private:
    QVector4D createFrustum(QMatrix4x4 mat, QVector3D p0, QVector3D p1, QVector3D p2);

    QVector3D m_defaultPosition;
    QVector3D m_position;
    QVector3D m_lookAt;
    QVector3D m_up = QVector3D(0, 1, 0);
    QQuaternion m_rotation;
    const float m_distance = 3;
    const float m_mouseSpeed = 0.002F;
    const float m_moveSpeed = 10.0F;
};

#endif // CAMERA_H
