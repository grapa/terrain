#ifndef HELLOGLWIDGET_H
#define HELLOGLWIDGET_H

#include "tree.h"
#include "camera.h"
#include "heightmap.h"
#include "timinggraph.h"
#include "debuglogger.h"
#include "errorchecker.h"
#include "worldgenerator.h"
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QTimer>

#define WORLD_SIZE 50
#define WORLD_NORMALIZATION_FACTOR (1.0F / 15000.0F)

struct Material
{
    float specularFactor[3];
    float shininessFactor;
};

struct Light
{
    QVector3D position = QVector3D(WORLD_SIZE / 2, 200, -WORLD_SIZE / 2);
    float intensity = 1;
    QVector3D color = QVector3D(1, 1, 1);
};

class HelloGLWidget : public QOpenGLWidget, protected ErrorChecker
{
    Q_OBJECT

public:
    HelloGLWidget(TimingGraph* timingGraph, QWidget *parent = 0);
    virtual ~HelloGLWidget() = default;

public:
    void setHeightMap(HeightMap* heightMap);
    void loadTree(QString fileName);
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

public slots:
    void handleTimeout();
    void toggleWireframe();
    void setLightX(int x);
    void setLightY(int y);
    void setLightZ(int z);
    void setLightIntensity(int i);
    void setLightR(int r);
    void setLightG(int g);
    void setLightB(int b);
    void updateVerticalScaling(int newValue);
    void updateHorizontalScaling(int newValue);
    void setNumberOfTrees(int trees);
    void setGeomDist(int dist);
    void setImpDist(int dist);
    void setRots(int rots);

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    void updateHeightMap();
    void loadTextures();
    void createMaterial(QVector3D specularFactor, float shininessFactor);
    void initComputeShader();
    void runComputeShader();
    void measureTime(unsigned int index);
    void reportTimings();
    float map(int i, float start1, float end1, float start2, float end2);

    DebugLogger* m_debugLogger;

    GLuint m_heightMapVao;
    HeightMap* m_heightMap = nullptr;
    bool m_updateHeightMap = false;
    GLuint m_heightMapTexture;
    float m_verticalScale = WORLD_NORMALIZATION_FACTOR;
    float m_horizontalScale = 10;
    int m_numberOfTrees = 100;
    float m_geomDist = 20;
    float m_impDist = 40;
    int m_rots = 36;
    bool m_updateRots = false;

    std::vector<Material> m_materials;
    std::vector<QImage> m_textures;
    bool m_updateTextures = false;
    GLuint m_arrayTexture;

    QOpenGLShaderProgram *m_treeShader;
    Tree *m_tree = nullptr;
    bool m_loadTree = false;
    bool m_treeLoaded = false;

    QOpenGLShaderProgram *m_impostorImageShader;
    QOpenGLShaderProgram *m_testShader;
    GLuint m_testVao;
    GLuint m_testVbo;
    GLuint m_testIbo;

    QOpenGLShaderProgram *m_computeShader;
    GLuint m_treeGeometryBuffer;
    GLuint m_treeImpostorBuffer;
    GLuint m_drawCommandBuffer;
    GLuint m_treeIndexBuffer;

    QOpenGLShaderProgram *m_impostorShader;

    QTimer m_timer;
    std::vector<std::pair<GLuint, GLuint>> m_queries;
    std::vector<GLuint64> m_queryResults;
    bool m_evenFrame = true;
    int m_frameNumber = 0;
    TimingGraph* m_timingGraph;

    QOpenGLShaderProgram* m_shader;
    bool m_showWireframe = false;
    World m_world = WorldGenerator().generateWorld(WORLD_SIZE);

    QMatrix4x4 m_p;

    Camera m_camera = Camera(QVector3D(WORLD_SIZE / 2, 10, -WORLD_SIZE / 2));
    Light m_lightSource;

    QPoint m_lastMousePos;
    bool m_mousePressed = false;

    bool m_wDown = false;
    bool m_aDown = false;
    bool m_sDown = false;
    bool m_dDown = false;
};

#endif // HELLOGLWIDGET_H
