#version 440

struct Material
{
    vec4 diffuseFactor;
    int diffuseTexture;
    float shininessFactor;
    int shininessTexture;
    int specularTexture;
    vec3 specularFactor;
};

in vec2 uv_FS;

out vec4 frag;

uniform int materialId;

layout(binding = 3) uniform sampler2DArray textureArray;

layout(std140, binding = 4) uniform MaterialBlock
{
    Material materials[256];
};

void main(void)
{
    Material material = materials[materialId];
    vec3 k_d = clamp(vec3(material.diffuseFactor), vec3(0), vec3(1));

    if (material.diffuseTexture >= 0)
    {
        vec4 texVal = texture(textureArray, vec3(uv_FS, material.diffuseTexture));
        if (texVal.a == 0)
            discard;

        k_d *= vec3(texVal);
    }

    frag = vec4(k_d, 1);
}
