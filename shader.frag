#version 440

struct Material
{
    vec3 k_s;
    float n;
};

in vec3 pos_FS;
in vec3 normal_FS;
in vec2 uv_FS;
in float height_FS;

out vec4 frag;

uniform vec3 cameraPos;
uniform vec3 lightPos;
uniform float lightIntensity;
uniform vec3 lightColor;
uniform int heightMapMaxValue;

layout(binding = 1) uniform sampler2DArray textureArray;

layout(std140, binding = 2) uniform MaterialBlock
{
    Material mats[4];
};

float map(float i, float start1, float end1, float start2, float end2)
{
    return (i - start1) / (end1 - start1) * (end2 - start2) + start2;
}

void main(void)
{
    vec4 weight;
    if (height_FS < heightMapMaxValue * 0.20) // sand
    {
        weight = vec4(0, 0, 1, 0);
    }
    else if (height_FS < heightMapMaxValue * 0.24) // sand to rock 0
    {
        float v = map(height_FS, heightMapMaxValue * 0.20, heightMapMaxValue * 0.24, 0, 1);
        weight = vec4(v, 0, 1 - v, 0);
    }
    else if (height_FS < heightMapMaxValue * 0.40) // rock 0
    {
        weight = vec4(1, 0, 0, 0);
    }
    else if (height_FS < heightMapMaxValue * 0.44) // rock 0 to rock 1
    {
        float v = map(height_FS, heightMapMaxValue * 0.40, heightMapMaxValue * 0.44, 0, 1);
        weight = vec4(1 - v, v, 0, 0);
    }
    else if (height_FS < heightMapMaxValue * 0.65) // rock 1
    {
        weight = vec4(0, 1, 0, 0);
    }
    else if (height_FS < heightMapMaxValue * 0.69) // rock 1 to stone
    {
        float v = map(height_FS, heightMapMaxValue * 0.65, heightMapMaxValue * 0.69, 0, 1);
        weight = vec4(0, 1 - v, 0, v);
    }
    else // stone
    {
        weight = vec4(0, 0, 0, 1);
    }

    weight.w = 1 - normal_FS.y;

    vec3 rock0 = vec3(texture(textureArray, vec3(uv_FS, 0)));
    vec3 rock1 = vec3(texture(textureArray, vec3(uv_FS, 1)));
    vec3 sand = vec3(texture(textureArray, vec3(uv_FS, 2)));
    vec3 stone = vec3(texture(textureArray, vec3(uv_FS, 3)));

    vec3 k_d = rock0 * weight.x + rock1 * weight.y + sand * weight.z + stone * weight.w;
    vec3 k_a = 0.1 * k_d;
    vec3 k_s = mats[0].k_s * weight.x + mats[1].k_s * weight.y + mats[2].k_s * weight.z + mats[3].k_s * weight.w;
    float n = mats[0].n * weight.x + mats[1].n * weight.y + mats[2].n * weight.z + mats[3].n * weight.w;
    float I_L = lightIntensity;

    vec3 V = normalize(pos_FS - cameraPos);
    vec3 N = normalize(normal_FS);
    vec3 L = normalize(lightPos - pos_FS);
    vec3 R = normalize(reflect(L, N));

    vec3 color = lightColor * (k_a * I_L + k_d * I_L * max(0, dot(N, L)) + k_s * I_L * pow(max(0, dot(R, -V)), n));
    frag = vec4(color, 1);
    //frag = vec4(normal_FS, 1);
}
