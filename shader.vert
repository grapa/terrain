#version 440

layout(location = 0) in vec3 pos_VS;
layout(location = 1) in vec2 uv_VS;

out vec3 pos_TC;
out vec2 uvHeightMap_TC;
out vec2 uvTexture_TC;

uniform mat4 m;
uniform vec2 cameraOffset;
uniform vec2 worldSize;
uniform vec2 heightMapSize;
uniform float horizontalScale;

void main(void)
{
    pos_TC = vec3(m * vec4(pos_VS, 1));
    uvHeightMap_TC = uv_VS * (worldSize * horizontalScale / heightMapSize) - cameraOffset / heightMapSize;
    uvTexture_TC = uv_VS;
}
