#ifndef TREE_H
#define TREE_H

#include "tinygltf/json.hpp"
#include "tinygltf/tiny_gltf.h"
#include <limits>
#include <QOpenGLFunctions_4_4_Core>
#include <QOpenGLShaderProgram>

struct TreeMaterial
{
    float diffuseFactor[4] = {0, 1, 0, 1};
    int diffuseTexture = -1;
    float shininessFactor = 10;
    int shininessTexture = -1;
    int specularTexture = -1;
    float specularFactor[3] = {1, 0, 1};
    char padding[4];
};

class Tree
{
public:
    Tree(QOpenGLFunctions_4_4_Core *gl);

    GLuint getPositionBuffer() const;
    int getTreeCount() const;
    std::vector<GLuint> getVertexCounts();
    QVector4D getBoundingSphere();

    void load(QString fileName);
    void initialize(unsigned int worldSize);
    void initImpostorImages(QOpenGLShaderProgram *shader, int rots);
    void initImpostorQuad(QOpenGLShaderProgram *shader);
    void draw(QOpenGLShaderProgram *shader, QMatrix4x4 m, GLuint drawCommandBuffer, GLuint treeGeometryBuffer, int amount);
    void drawImpostor(GLuint drawCommandBuffer, GLuint treeImpostorBuffer);
    void bindGBuffer();

private:
    void loadBufferViews();
    void loadMaterials();
    void loadTextures();
    void loadInstancePositions(unsigned int worldSize);
    void loadMesh(tinygltf::Mesh &mesh);
    void bindInstancePositions();
    void drawModelNodes(tinygltf::Node &node, QOpenGLShaderProgram *shader, GLuint drawCommandBuffer, GLuint treeGeometryBuffer, int amount);
    void drawMesh(tinygltf::Mesh &mesh, QOpenGLShaderProgram *shader, GLuint drawCommandBuffer, GLuint treeGeometryBuffer, int amount);
    void drawPrimitiveDirect(tinygltf::Mesh &mesh, tinygltf::Primitive &primitive, unsigned int i, QOpenGLShaderProgram *shader);
    void drawPrimitiveIndirect(tinygltf::Mesh &mesh, tinygltf::Primitive &primitive, unsigned int i, QOpenGLShaderProgram *shader, GLuint drawCommandBuffer, GLuint treeGeometryBuffer);

    QOpenGLFunctions_4_4_Core *m_gl;

    std::map<unsigned int, GLuint> m_vbos;
    GLuint m_instanceVbo;
    std::map<std::string, std::vector<GLuint>> m_vaos;
    std::vector<GLuint> m_vertexCounts;
    QVector4D m_boundingSphere;
    QVector3D m_minBB = QVector3D(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
    QVector3D m_maxBB = QVector3D(std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min());
    GLuint m_ubo;
    std::vector<QMatrix4x4> m_ms;

    GLuint m_gBuffer;
    GLuint m_fbo;

    GLuint m_vao;
    GLuint m_vbo;
    GLuint m_tbo;
    GLuint m_ibo;

    tinygltf::Model *m_model = nullptr;
    TreeMaterial *m_materials = nullptr;
};

#endif // TREE_H
