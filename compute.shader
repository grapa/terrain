#version 430

struct IndirectDrawCommand
{
    uint count;
    uint primCount;
    uint firstIndex;
    uint baseVertex;
    uint baseInstance;
};

uniform vec4 frustumR;
uniform vec4 frustumL;
uniform vec4 frustumT;
uniform vec4 frustumB;
uniform vec4 frustumN;
uniform vec4 frustumF;
uniform vec4 bSphere;
uniform mat4 m;
uniform vec4 vertexCounts;
uniform vec3 cameraPos;
uniform int numberOfTrees;
uniform float geometryDistance;
uniform float impostorDistance;

layout(std430, binding = 0) buffer TreeDataIn
{
    vec4 treeDataIn[];
};

layout(std430, binding = 1) buffer IndirectDraw
{
    IndirectDrawCommand commands[];
};

layout(std430, binding = 2) buffer IndexDataIn
{
    uint globalReadIndex;
    uint globalGeometryIndex;
    uint globalImpostorIndex;
};

layout(std430, binding = 3) buffer GeometryDataOut
{
    vec4 geometryDataOut[];
};

layout(std430, binding = 4) buffer ImpostorDataOut
{
    vec4 impostorDataOut[];
};

layout(local_size_x = 16) in;

bool cull(vec3 pos, vec4 frustum, float r)
{
    return dot(pos + vec3(bSphere), vec3(frustum)) + frustum.w < r;
}

void main()
{
    if (gl_GlobalInvocationID.x == 0 || gl_GlobalInvocationID.y == 0 || gl_GlobalInvocationID.z == 0)
    {
        uint counts[5] = {6, uint(vertexCounts.x), uint(vertexCounts.y),
                          uint(vertexCounts.z), uint(vertexCounts.w)};
        for (int i = 0; i < 5; i++)
        {
            commands[i].count = counts[i];
            commands[i].primCount = 0;
            commands[i].firstIndex = 0;
            commands[i].baseVertex = 0;
            commands[i].baseInstance = 0;
        }

        globalReadIndex = 0;
        globalGeometryIndex = 0;
        globalImpostorIndex = 0;
    }

    memoryBarrierShared();
    barrier();

    uint inI = atomicAdd(globalReadIndex, 1);
    if (inI >= treeDataIn.length() || inI >= numberOfTrees)
        return;

    vec4 treeData = treeDataIn[inI];
    vec3 pos = vec3(m * vec4(treeData.x, 0, treeData.y, 1));
    float dist = length(pos - cameraPos);
    float r = -treeData.z * bSphere.w;

    bool visible = cull(pos, frustumR, r);
    visible = visible || cull(pos, frustumL, r);
    visible = visible || cull(pos, frustumT, r);
    visible = visible || cull(pos, frustumB, r);
    visible = visible || cull(pos, frustumN, r);
    visible = visible || cull(pos, frustumF, r);

    if (visible)
    {
        if (dist < geometryDistance)
        {
            uint outI = atomicAdd(globalGeometryIndex, 1);
            geometryDataOut[outI] = treeData;

            for (int i = 0; i < 4; i++)
            {
                atomicAdd(commands[i + 1].primCount, 1);
            }
        }
        else if (dist < impostorDistance)
        {
            uint outI = atomicAdd(globalImpostorIndex, 1);
            impostorDataOut[outI] = treeData;
            atomicAdd(commands[0].primCount, 1);
        }
    }

    memoryBarrierShared();
    barrier();
}
