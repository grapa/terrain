#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define _USE_MATH_DEFINES

#include "tree.h"
#include <math.h>
#include <iostream>

const unsigned int numTris = 2;
const unsigned int numVerts = 4;

const GLfloat vertexPosition[3 * numVerts] =
{
    0, 0, 0.0,
    0, 1.0, 0.0,
    1.0, 0, 0.0,
    1.0, 1.0, 0.0
};

const GLuint vertexIndex[3 * numTris] =
{
    0, 1, 2,
    1, 3, 2
};

Tree::Tree(QOpenGLFunctions_4_4_Core *gl) : m_gl(gl)
{

}

GLuint Tree::getPositionBuffer() const
{
    return m_instanceVbo;
}

int Tree::getTreeCount() const
{
    return 100;
}

std::vector<GLuint> Tree::getVertexCounts()
{
    return m_vertexCounts;
}

QVector4D Tree::getBoundingSphere()
{
    return m_boundingSphere;
}

void Tree::load(QString fileName)
{
    if (m_model != nullptr)
        delete m_model;

    m_model = new tinygltf::Model();

    tinygltf::TinyGLTF loader;
    std::string error;
    std::string warning;

    bool result = loader.LoadASCIIFromFile(m_model, &error, &warning, fileName.toUtf8().constData());
    if (!error.empty())
    {
        std::cout << "Error: " << error.c_str() << std::endl;
        return;
    }

    if (!warning.empty())
    {
        std::cout << "Warning: " << warning.c_str() << std::endl;
        return;
    }

    if (!result)
    {
        std::cout << "Failed to parse glTF." << std::endl;
        return;
    }
}

void Tree::initialize(unsigned int worldSize)
{
    m_gl->glGenFramebuffers(1, &m_fbo);
    m_gl->glGenTextures(1, &m_gBuffer);

    m_gl->glBindVertexArray(0);

    loadBufferViews();
    loadMaterials();
    loadTextures();
    loadInstancePositions(worldSize);

    for (unsigned int i = 0; i < m_model->meshes.size(); i++)
    {
        tinygltf::Mesh &mesh = m_model->meshes[i];
        loadMesh(mesh);
    }

    QVector3D bbSphereCenter = (m_minBB + m_maxBB) / 2;
    m_boundingSphere = QVector4D(bbSphereCenter, (bbSphereCenter - m_maxBB).length());
}

void Tree::loadBufferViews()
{
    for (unsigned int i = 0; i < m_model->bufferViews.size(); i++)
    {
        const tinygltf::BufferView &bufferView = m_model->bufferViews[i];
        const tinygltf::Buffer &buffer = m_model->buffers[static_cast<unsigned long long>(bufferView.buffer)];

        GLuint vbo;
        m_gl->glGenBuffers(1, &vbo);
        m_vbos[i] = vbo;

        m_gl->glBindBuffer(static_cast<GLuint>(bufferView.target), m_vbos[i]);
        m_gl->glBufferData(static_cast<GLuint>(bufferView.target), static_cast<long long>(bufferView.byteLength), &buffer.data.at(0) + bufferView.byteOffset, GL_STATIC_DRAW);
        m_gl->glBindBuffer(static_cast<GLuint>(bufferView.target), 0);
    }
}

void Tree::loadMaterials()
{
    if (m_materials != nullptr)
        delete [] m_materials;

    m_materials = new TreeMaterial[m_model->materials.size()];

    for (unsigned int i = 0; i < m_model->materials.size(); i++)
    {
        tinygltf::Material &mat = m_model->materials[i];
        TreeMaterial material;

        auto blinnPhongQuery = mat.extensions.find("KHR_materials_cmnBlinnPhong");
        if (blinnPhongQuery != mat.extensions.end())
        {
            tinygltf::Value &blinnPhongData = blinnPhongQuery->second;

            if (blinnPhongData.Has("diffuseFactor"))
            {
                for (int i = 0; i < 4; i++)
                {
                    material.diffuseFactor[i] = static_cast<float>(blinnPhongData.Get("diffuseFactor").Get(i).Get<double>());
                }
            }

            if (blinnPhongData.Has("diffuseTexture"))
            {
                material.diffuseTexture = blinnPhongData.Get("diffuseTexture").Get("index").Get<int>();
            }

            if (blinnPhongData.Has("specularFactor"))
            {
                for (int i = 0; i < 3; i++)
                {
                    material.specularFactor[i] = static_cast<float>(blinnPhongData.Get("specularFactor").Get(i).Get<double>());
                }
            }

            if (blinnPhongData.Has("specularTexture"))
            {
                material.specularTexture = blinnPhongData.Get("specularTexture").Get("index").Get<int>();
            }

            if (blinnPhongData.Has("shininessFactor"))
            {
                material.shininessFactor = static_cast<float>(blinnPhongData.Get("shininessFactor").Get<double>());
            }

            if (blinnPhongData.Has("shininessTexture"))
            {
                material.shininessTexture = blinnPhongData.Get("shininessTexture").Get("index").Get<int>();
            }
        }

        m_materials[i] = material;
    }

    m_gl->glGenBuffers(1, &m_ubo);
    m_gl->glBindBuffer(GL_UNIFORM_BUFFER, m_ubo);
    m_gl->glBufferData(GL_UNIFORM_BUFFER, static_cast<GLsizeiptr>(m_model->materials.size() * sizeof(TreeMaterial)), m_materials, GL_STATIC_DRAW);
    m_gl->glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Tree::loadTextures()
{
    GLuint textureId;
    m_gl->glGenTextures(1, &textureId);
    m_gl->glActiveTexture(GL_TEXTURE3);
    m_gl->glBindTexture(GL_TEXTURE_2D_ARRAY, textureId);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    for (unsigned int i = 0; i < m_model->textures.size(); i++)
    {
        tinygltf::Texture &texture = m_model->textures[i];
        tinygltf::Image &image = m_model->images[static_cast<unsigned long long>(texture.source)];

        if (i == 0)
        {
            m_gl->glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, image.width, image.height, static_cast<int>(m_model->textures.size()));
        }

        GLenum format = GL_RGBA;
        if (image.component == 1)
        {
            format = GL_RED;
        }
        else if (image.component == 2)
        {
            format = GL_RG;
        }
        else if (image.component == 3)
        {
            format = GL_RGB;
        }

        GLenum type = GL_UNSIGNED_BYTE;
        if (image.bits == 16)
        {
            type = GL_UNSIGNED_SHORT;
        }

        m_gl->glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, static_cast<int>(i), image.width, image.height, 1, format, type, image.image.data());
    }
}

void Tree::loadInstancePositions(unsigned int worldSize)
{
    std::vector<QVector4D> positions;
    for (unsigned int y = 0; y < worldSize / 5; y++)
    {
        for (unsigned int x = 0; x < worldSize / 5; x++)
        {
            QVector4D position;
            position.setX(static_cast<float>(x) * 5);
            position.setY(-static_cast<float>(y) * 5);
            position.setZ((rand() % 5) + 2);
            position.setW(static_cast<float>((rand() % 360) * M_PI / 180));
            positions.push_back(position);
        }
    }

    m_gl->glGenBuffers(1, &m_instanceVbo);
    m_gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_instanceVbo);
    m_gl->glBufferData(GL_SHADER_STORAGE_BUFFER, static_cast<GLsizeiptr>(positions.size() * sizeof(QVector4D)), positions.data(), GL_STATIC_DRAW);
    m_gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void Tree::loadMesh(tinygltf::Mesh &mesh)
{
    m_vaos[mesh.name] = std::vector<GLuint>();

    for (unsigned int i = 0; i < mesh.primitives.size(); i++)
    {
        GLuint vao;
        m_gl->glGenVertexArrays(1, &vao);
        m_gl->glBindVertexArray(vao);
        m_vaos[mesh.name].push_back(vao);

        tinygltf::Primitive &primitive = mesh.primitives[i];
        tinygltf::Accessor &indexAccessor = m_model->accessors[static_cast<unsigned long long>(primitive.indices)];
        m_vertexCounts.push_back(static_cast<GLuint>(indexAccessor.count));

        for (auto &attribute : primitive.attributes)
        {
            const tinygltf::Accessor accessor = m_model->accessors[static_cast<unsigned long long>(attribute.second)];
            int byteStride = accessor.ByteStride(m_model->bufferViews[static_cast<unsigned long long>(accessor.bufferView)]);
            m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_vbos[static_cast<unsigned int>(accessor.bufferView)]);

            int size = 1;
            if (accessor.type != TINYGLTF_TYPE_SCALAR)
            {
                size = accessor.type;
            }

            int vaa = -1;
            if (attribute.first.compare("POSITION") == 0)
            {
                vaa = 0;

                m_minBB.setX(std::min(m_minBB.x(), static_cast<float>(accessor.minValues.at(0))));
                m_minBB.setY(std::min(m_minBB.y(), static_cast<float>(accessor.minValues.at(1))));
                m_minBB.setZ(std::min(m_minBB.z(), static_cast<float>(accessor.minValues.at(2))));

                m_maxBB.setX(std::max(m_maxBB.x(), static_cast<float>(accessor.maxValues.at(0))));
                m_maxBB.setY(std::max(m_maxBB.y(), static_cast<float>(accessor.maxValues.at(1))));
                m_maxBB.setZ(std::max(m_maxBB.z(), static_cast<float>(accessor.maxValues.at(2))));
            }
            if (attribute.first.compare("NORMAL") == 0)
                vaa = 1;
            if (attribute.first.compare("TEXCOORD_0") == 0)
                vaa = 2;
            if (vaa > -1)
            {
                m_gl->glEnableVertexAttribArray(static_cast<unsigned int>(vaa));
                m_gl->glVertexAttribPointer(static_cast<unsigned int>(vaa), size, static_cast<unsigned int>(accessor.componentType),
                                      accessor.normalized ? GL_TRUE : GL_FALSE, byteStride, reinterpret_cast<GLvoid*>(accessor.byteOffset));
            }
        }

        bindInstancePositions();
    }

    m_gl->glBindVertexArray(0);
}

void Tree::bindInstancePositions()
{
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_instanceVbo);
    m_gl->glEnableVertexAttribArray(3);
    m_gl->glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), nullptr);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, 0);
    m_gl->glVertexAttribDivisor(3, 1);
}

void Tree::initImpostorImages(QOpenGLShaderProgram *shader, int rots)
{
    m_gl->glEnable(GL_DEPTH_TEST);
    m_gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_gl->glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
    m_gl->glDrawBuffer(GL_COLOR_ATTACHMENT0);

    m_gl->glActiveTexture(GL_TEXTURE5);
    m_gl->glBindTexture(GL_TEXTURE_2D_ARRAY, m_gBuffer);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BASE_LEVEL, 0);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_LEVEL, 0);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    m_gl->glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    m_gl->glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA8, 500, 190, rots, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    for (int i = 0; i < rots; i++)
    {
        m_gl->glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_gBuffer, 0, i);
        m_gl->glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

        float angle = static_cast<float>(i * (360.0 / rots) * M_PI / 180);
        shader->setUniformValue("angle", angle);

        draw(shader, QMatrix4x4(), 0, 0, 1);
    }

    m_gl->glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Tree::initImpostorQuad(QOpenGLShaderProgram *shader)
{
    m_gl->glGenVertexArrays(1, &m_vao);
    m_gl->glBindVertexArray(m_vao);

    m_gl->glGenBuffers(1, &m_vbo);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    m_gl->glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), vertexPosition, GL_STATIC_DRAW);
    shader->enableAttributeArray(0);
    shader->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_gl->glGenBuffers(1, &m_tbo);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, m_tbo);
    m_gl->glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), vertexPosition, GL_STATIC_DRAW);
    shader->enableAttributeArray(2);
    shader->setAttributeBuffer(2, GL_FLOAT, 0, 3);
    m_gl->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_gl->glGenBuffers(1, &m_ibo);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    m_gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * numTris * sizeof(GLuint), vertexIndex, GL_STATIC_DRAW);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    m_gl->glBindVertexArray(0);
}

void Tree::draw(QOpenGLShaderProgram *shader, QMatrix4x4 m, GLuint drawCommandBuffer, GLuint treeGeometryBuffer, int amount)
{
    if (amount > 1)
    {
        m_gl->glBindBuffer(GL_DRAW_INDIRECT_BUFFER, drawCommandBuffer);
    }

    m_gl->glBindBufferBase(GL_UNIFORM_BUFFER, 4, m_ubo);

    tinygltf::Scene &scene = m_model->scenes[static_cast<unsigned long long>(m_model->defaultScene)];
    for (unsigned int i = 0; i < scene.nodes.size(); i++)
    {
        m_ms.clear();
        m_ms.push_back(m);

        const unsigned int nodeIndex = static_cast<unsigned int>(scene.nodes[i]);
        drawModelNodes(m_model->nodes[nodeIndex], shader, drawCommandBuffer, treeGeometryBuffer, amount);
    }
}

void Tree::drawModelNodes(tinygltf::Node &node, QOpenGLShaderProgram *shader, GLuint drawCommandBuffer, GLuint treeGeometryBuffer, int amount)
{
    QMatrix4x4 m = m_ms.at(m_ms.size() - 1);
    if (node.translation.size() == 3)
    {
        double *translation = node.translation.data();
        m.translate(static_cast<float>(translation[0]), static_cast<float>(translation[1]),
                static_cast<float>(translation[2]));
    }
    if (node.rotation.size() == 4)
    {
        double *rotation = node.rotation.data();
        m.rotate(QQuaternion(static_cast<float>(rotation[3]), static_cast<float>(rotation[0]),
                static_cast<float>(rotation[1]), static_cast<float>(rotation[2])).normalized());
    }
    if (node.scale.size() == 3)
    {
        double *scale = node.scale.data();
        m.scale(static_cast<float>(scale[0]), static_cast<float>(scale[1]),
                static_cast<float>(scale[2]));
    }
    if (node.matrix.size() == 16)
    {
        double *mat = node.matrix.data();
        m = QMatrix4x4(static_cast<float>(mat[0]), static_cast<float>(mat[1]), static_cast<float>(mat[2]), static_cast<float>(mat[3]),
                static_cast<float>(mat[4]), static_cast<float>(mat[5]), static_cast<float>(mat[6]), static_cast<float>(mat[7]),
                static_cast<float>(mat[8]), static_cast<float>(mat[9]), static_cast<float>(mat[10]), static_cast<float>(mat[11]),
                static_cast<float>(mat[12]), static_cast<float>(mat[13]), static_cast<float>(mat[14]), static_cast<float>(mat[15])) * m;
    }

    m_ms.push_back(m);

    if (node.mesh >= 0)
    {
        drawMesh(m_model->meshes[static_cast<unsigned long long>(node.mesh)], shader, drawCommandBuffer, treeGeometryBuffer, amount);
    }

    for (unsigned int i = 0; i < node.children.size(); i++)
    {
        drawModelNodes(m_model->nodes[static_cast<unsigned int>(node.children[i])], shader, drawCommandBuffer, treeGeometryBuffer, amount);
    }

    m_ms.pop_back();
}

void Tree::drawMesh(tinygltf::Mesh &mesh, QOpenGLShaderProgram *shader, GLuint drawCommandBuffer, GLuint treeGeometryBuffer, int amount)
{
    for (unsigned int i = 0; i < mesh.primitives.size(); i++)
    {
        tinygltf::Primitive primitive = mesh.primitives[i];
        if (amount == 1)
        {
            drawPrimitiveDirect(mesh, primitive, i, shader);
        }
        else
        {
            drawPrimitiveIndirect(mesh, primitive, i, shader, drawCommandBuffer, treeGeometryBuffer);
        }
    }
}

void Tree::drawPrimitiveDirect(tinygltf::Mesh &mesh, tinygltf::Primitive &primitive, unsigned int i, QOpenGLShaderProgram *shader)
{
    tinygltf::Accessor accessor = m_model->accessors[static_cast<unsigned long long>(primitive.indices)];

    m_gl->glBindVertexArray(m_vaos[mesh.name].at(i));

    const tinygltf::BufferView &bufferView = m_model->bufferViews[static_cast<unsigned long long>(accessor.bufferView)];
    m_gl->glBindBuffer(static_cast<GLuint>(bufferView.target), m_vbos[static_cast<unsigned int>(accessor.bufferView)]);

    QMatrix4x4 &m = m_ms.at(m_ms.size() - 1);
    shader->setUniformValue("m", m);
    shader->setUniformValue("n", m.inverted().transposed());
    shader->setUniformValue("materialId", primitive.material);

    m_gl->glDrawElements(static_cast<GLenum>(primitive.mode), static_cast<int>(accessor.count), static_cast<GLenum>(accessor.componentType),
                         reinterpret_cast<GLvoid*>(accessor.byteOffset));

    m_gl->glBindVertexArray(0);
}

void Tree::drawPrimitiveIndirect(tinygltf::Mesh &mesh, tinygltf::Primitive &primitive, unsigned int i, QOpenGLShaderProgram *shader, GLuint drawCommandBuffer, GLuint treeGeometryBuffer)
{
    tinygltf::Accessor accessor = m_model->accessors[static_cast<unsigned long long>(primitive.indices)];

    m_gl->glBindVertexArray(m_vaos[mesh.name].at(i));
    m_gl->glBindBuffer(GL_DRAW_INDIRECT_BUFFER, drawCommandBuffer);

    m_gl->glBindBuffer(GL_ARRAY_BUFFER, treeGeometryBuffer);
    m_gl->glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    m_gl->glEnableVertexAttribArray(3);
    m_gl->glVertexAttribDivisor(3, 1);

    QMatrix4x4 &m = m_ms.at(m_ms.size() - 1);
    shader->setUniformValue("m", m);
    shader->setUniformValue("n", m.inverted().transposed());
    shader->setUniformValue("materialId", primitive.material);

    m_gl->glDrawElementsIndirect(static_cast<GLenum>(primitive.mode), static_cast<GLenum>(accessor.componentType),
                                 reinterpret_cast<GLvoid*>(5 * (i + 1) * sizeof(GLuint)));

    m_gl->glBindVertexArray(0);
}

void Tree::drawImpostor(GLuint drawCommandBuffer, GLuint treeImpostorBuffer)
{
    bindGBuffer();

    m_gl->glBindVertexArray(m_vao);
    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    m_gl->glBindBuffer(GL_DRAW_INDIRECT_BUFFER, drawCommandBuffer);

    m_gl->glBindBuffer(GL_ARRAY_BUFFER, treeImpostorBuffer);
    m_gl->glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    m_gl->glEnableVertexAttribArray(3);
    m_gl->glVertexAttribDivisor(3, 1);

    m_gl->glDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_INT, reinterpret_cast<GLvoid*>(0));

    m_gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    m_gl->glBindVertexArray(0);
}

void Tree::bindGBuffer()
{
    m_gl->glActiveTexture(GL_TEXTURE5);
    m_gl->glBindTexture(GL_TEXTURE_2D_ARRAY, m_gBuffer);
}
