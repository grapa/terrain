#version 440

#define PI 3.14159

layout(location = 0) in vec3 pos_VS;
layout(location = 2) in vec2 uv_VS;
layout(location = 3) in vec4 offset_VS;

out vec2 uv_FS;
flat out int i_FS;

uniform mat4 m;
uniform mat4 v;
uniform mat4 p;
uniform mat4 n;
uniform vec3 cameraPos;
uniform int angles;

mat4 rotMat(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1 - c;

    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
                oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
                oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
                0.0, 0.0, 0.0, 1.0);
}

void main(void)
{
    vec3 P = vec3(m * vec4(pos_VS, 1)) + vec3(offset_VS.x, 0, offset_VS.z);
    vec3 sourceNormal = vec3(0, 0, -1);
    vec3 targetNormal = normalize(cameraPos - vec3(P.x, cameraPos.y, P.z));
    float angle = atan(targetNormal.x, targetNormal.z) - atan(sourceNormal.x, sourceNormal.z);
    float angleDeg = mod(offset_VS.w - angle * 180.0 / PI, 360.0);

    i_FS = int(angles * angleDeg / 360);

    vec3 pos = pos_VS * offset_VS.z;
    pos = vec3(rotMat(vec3(0, 1, 0), angle) * vec4(pos, 1));

    pos.x += offset_VS.x;
    pos.z += offset_VS.y;

    uv_FS = uv_VS;
    gl_Position = (p * v * m) * vec4(pos, 1);
}
