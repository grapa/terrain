#version 440

layout(vertices = 4) out;

in vec3 pos_TC[];
in vec2 uvHeightMap_TC[];
in vec2 uvTexture_TC[];

out vec3 pos_TE[];
out vec2 uvHeightMap_TE[];
out vec2 uvTexture_TE[];
out float mipmapLevel_TE[];

uniform vec3 cameraPos;

int getTessLevel(float dist0, float dist1)
{
    float avgDist = (dist0 + dist1) / 2.0F;
    if (avgDist <= 5) return 16;
    if (avgDist <= 10) return 8;
    if (avgDist <= 20) return 4;
    //if (avgDist <= 100) return 2;
    return 2;
}

float getMipmapLevel()
{
    int tessLevel = int(gl_TessLevelOuter[gl_InvocationID]);
    if (tessLevel == 16) return 0;
    if (tessLevel == 8) return 1;
    if (tessLevel == 4) return 2;
    return 3;
}

void main(void)
{
    pos_TE[gl_InvocationID] = pos_TC[gl_InvocationID];
    uvHeightMap_TE[gl_InvocationID] = uvHeightMap_TC[gl_InvocationID];
    uvTexture_TE[gl_InvocationID] = uvTexture_TC[gl_InvocationID];

    float viewDist0 = distance(cameraPos.xz, pos_TC[0].xz);
    float viewDist1 = distance(cameraPos.xz, pos_TC[1].xz);
    float viewDist2 = distance(cameraPos.xz, pos_TC[2].xz);
    float viewDist3 = distance(cameraPos.xz, pos_TC[3].xz);

    gl_TessLevelOuter[0] = getTessLevel(viewDist0, viewDist2);
    gl_TessLevelOuter[1] = getTessLevel(viewDist0, viewDist1);
    gl_TessLevelOuter[2] = getTessLevel(viewDist1, viewDist3);
    gl_TessLevelOuter[3] = getTessLevel(viewDist2, viewDist3);

    float inner = max(gl_TessLevelOuter[0], max(gl_TessLevelOuter[1], max(gl_TessLevelOuter[2], gl_TessLevelOuter[3])));
    gl_TessLevelInner[0] = inner;
    gl_TessLevelInner[1] = inner;

    mipmapLevel_TE[gl_InvocationID] = getMipmapLevel();
}
