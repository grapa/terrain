#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include <QWidget>

struct MipMap
{
    int width;
    int height;
    std::vector<quint16> *data;
};

class HeightMap
{
public:
    HeightMap(QString &fileName);

    int width();
    int height();
    quint16 max();

    std::vector<MipMap> *mipmaps();

private:
    int m_width;
    int m_height;
    quint16 m_max;

    std::vector<MipMap> m_maps;
};

#endif // HEIGHTMAP_H
