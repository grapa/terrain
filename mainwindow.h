#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "helloglwidget.h"
#include "heightmap.h"
#include "timinggraph.h"
#include <QMainWindow>
#include <QtWidgets>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow() = default;

public slots:
    void openFile();
    void loadTree();
    void showAboutBox();

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    HelloGLWidget* m_glWidget;
    TimingGraph* m_timingGraph;
};

#endif // MAINWINDOW_H
