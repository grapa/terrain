#include "helloglwidget.h"
#include <iostream>

//#define DEBUG_TREE

const unsigned int numTris = 2;
const unsigned int numVerts = 4;

const GLfloat vertexPosition[3 * numVerts] =
{
    -1.0, -1.0, 0.0,
    -1.0, 1.0, 0.0,
    1.0, -1.0, 0.0,
    1.0, 1.0, 0.0
};

const GLuint vertexIndex[3 * numTris] =
{
    0, 1, 2,
    1, 3, 2
};

HelloGLWidget::HelloGLWidget(TimingGraph* timingGraph, QWidget *parent)
    : QOpenGLWidget(parent), m_timingGraph(timingGraph)
{
    m_timer.setInterval(1000 / 30);
    connect(&m_timer, &QTimer::timeout, this, &HelloGLWidget::handleTimeout);
}

void HelloGLWidget::setHeightMap(HeightMap *heightMap)
{
    m_heightMap = heightMap;
    m_updateHeightMap = true;
}

void HelloGLWidget::loadTree(QString fileName)
{
    if (m_tree != nullptr)
        delete m_tree;

    m_tree = new Tree(this);
    m_tree->load(fileName);
    m_loadTree = true;
}

void HelloGLWidget::toggleWireframe()
{
    m_showWireframe = !m_showWireframe;
}

void HelloGLWidget::setLightX(int x)
{
    m_lightSource.position.setX(x);
}

void HelloGLWidget::setLightY(int y)
{
    m_lightSource.position.setY(y);
}

void HelloGLWidget::setLightZ(int z)
{
    m_lightSource.position.setZ(z);
}

void HelloGLWidget::setLightIntensity(int i)
{
    m_lightSource.intensity = i;
}

void HelloGLWidget::setLightR(int r)
{
    m_lightSource.color.setX(map(r, 0, 255, 0, 1));
}

void HelloGLWidget::setLightG(int g)
{
    m_lightSource.color.setY(map(g, 0, 255, 0, 1));
}

void HelloGLWidget::setLightB(int b)
{
    m_lightSource.color.setZ(map(b, 0, 255, 0, 1));
}

void HelloGLWidget::updateVerticalScaling(int newValue)
{
    m_verticalScale = newValue * WORLD_NORMALIZATION_FACTOR;
}

void HelloGLWidget::updateHorizontalScaling(int newValue)
{
    if (m_heightMap != nullptr)
    {
        m_horizontalScale = map(newValue, 1, 50, 10, 1.0F * m_heightMap->width() / WORLD_SIZE);
    }
}

void HelloGLWidget::setNumberOfTrees(int trees)
{
    m_numberOfTrees = trees;
}

void HelloGLWidget::setGeomDist(int dist)
{
    m_geomDist = dist;
}

void HelloGLWidget::setImpDist(int dist)
{
    m_impDist = dist;
}

void HelloGLWidget::setRots(int rots)
{
    m_rots = rots;
    m_updateRots = true;
}

void HelloGLWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_W:
        m_wDown = true;
        break;
    case Qt::Key_A:
        m_aDown = true;
        break;
    case Qt::Key_S:
        m_sDown = true;
        break;
    case Qt::Key_D:
        m_dDown = true;
        break;
    }
}

void HelloGLWidget::keyReleaseEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_W:
        m_wDown = false;
        break;
    case Qt::Key_A:
        m_aDown = false;
        break;
    case Qt::Key_S:
        m_sDown = false;
        break;
    case Qt::Key_D:
        m_dDown = false;
        break;
    }
}

void HelloGLWidget::handleTimeout()
{
    update();
}

void HelloGLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(0.5F, 0.5F, 0.5F, 1.0F);
    glEnable(GL_DEPTH_TEST);

    m_debugLogger = new DebugLogger();

    m_shader = new QOpenGLShaderProgram();
    m_shader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shader.vert");
    m_shader->addShaderFromSourceFile(QOpenGLShader::TessellationControl, ":/tc.shader");
    m_shader->addShaderFromSourceFile(QOpenGLShader::TessellationEvaluation, ":/te.shader");
    m_shader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shader.frag");
    m_shader->link();

    m_treeShader = new QOpenGLShaderProgram();
    m_treeShader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/tree.vert");
    m_treeShader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/tree.frag");
    m_treeShader->link();

    m_impostorImageShader = new QOpenGLShaderProgram();
    m_impostorImageShader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/impostorimage.vert");
    m_impostorImageShader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/impostorimage.frag");
    m_impostorImageShader->link();

    m_computeShader = new QOpenGLShaderProgram();
    m_computeShader->addShaderFromSourceFile(QOpenGLShader::Compute, ":/compute.shader");
    m_computeShader->link();

    m_impostorShader = new QOpenGLShaderProgram();
    m_impostorShader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/impostor.vert");
    m_impostorShader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/impostor.frag");
    m_impostorShader->link();

    m_testShader = new QOpenGLShaderProgram();
    m_testShader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/test.vert");
    m_testShader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/test.frag");
    m_testShader->link();
    m_testShader->bind();

    glGenVertexArrays(1, &m_testVao);
    glBindVertexArray(m_testVao);

    glGenBuffers(1, &m_testVbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_testVbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * numVerts * sizeof(GLfloat), vertexPosition, GL_STATIC_DRAW);
    m_testShader->enableAttributeArray(4);
    m_testShader->setAttributeBuffer(4, GL_FLOAT, 0, 3);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &m_testIbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_testIbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * numTris * sizeof(GLuint), vertexIndex, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    glGenVertexArrays(1, &m_heightMapVao);
    glBindVertexArray(m_heightMapVao);

    m_shader->bind();

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, 3 * m_world.numVertices * sizeof(GLfloat), m_world.vertices, GL_STATIC_DRAW);
    m_shader->enableAttributeArray(0);
    m_shader->setAttributeBuffer(0, GL_FLOAT, 0, 3);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    GLuint tbo;
    glGenBuffers(1, &tbo);
    glBindBuffer(GL_ARRAY_BUFFER, tbo);
    glBufferData(GL_ARRAY_BUFFER, 2 * m_world.numVertices * sizeof(GLfloat), m_world.textureCoords, GL_STATIC_DRAW);
    m_shader->enableAttributeArray(1);
    m_shader->setAttributeBuffer(1, GL_FLOAT, 0, 2);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    GLuint ibo;
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * m_world.numPatches * sizeof(GLuint), m_world.indices, GL_STATIC_DRAW);

    glGenTextures(1, &m_heightMapTexture);
    glGenTextures(1, &m_arrayTexture);
    loadTextures();

    GLuint ubo;
    glGenBuffers(1, &ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, ubo);
    glBufferData(GL_UNIFORM_BUFFER, static_cast<GLsizeiptr>(m_materials.size() * sizeof(Material)), m_materials.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, ubo);

    for (unsigned int i = 0; i < 6; i++)
    {
        std::vector<GLuint> vector(2);
        glGenQueries(2, vector.data());
        m_queries.push_back(std::pair<GLuint, GLuint>(vector[0], vector[1]));
    }

    glGenBuffers(1, &m_treeGeometryBuffer);
    glGenBuffers(1, &m_treeImpostorBuffer);
    glGenBuffers(1, &m_drawCommandBuffer);
    glGenBuffers(1, &m_treeIndexBuffer);

    m_timer.start();
}

void HelloGLWidget::paintGL()
{
    m_queryResults.clear();
    measureTime(0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_updateHeightMap)
    {
        m_updateHeightMap = false;
        updateHeightMap();
    }

    if (m_loadTree)
    {
        m_loadTree = false;

        m_treeShader->bind();
        m_tree->initialize(WORLD_SIZE);

        m_impostorShader->bind();
        m_tree->initImpostorQuad(m_impostorShader);

        m_computeShader->bind();
        initComputeShader();

        m_treeLoaded = true;
        m_updateRots = true;
    }

    if (m_updateRots)
    {
        m_impostorImageShader->bind();
        m_tree->initImpostorImages(m_impostorImageShader, m_rots);
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebufferObject());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        m_updateRots = false;
    }
#ifdef DEBUG_TREE
    m_testShader->bind();
    if (m_treeLoaded)
    {
        m_tree->bindGBuffer();
    }
    glBindVertexArray(m_testVao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_testIbo);
    glDrawElements(GL_TRIANGLES, 3 * numTris, GL_UNSIGNED_INT, nullptr);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
#else
    m_camera.move(m_wDown, m_aDown, m_sDown, m_dDown);

    m_shader->bind();
    m_shader->setUniformValue("m", m_camera.getModelMatrix());
    m_shader->setUniformValue("v", m_camera.getViewMatrix());
    m_shader->setUniformValue("p", m_p);
    m_shader->setUniformValue("cameraPos", m_camera.getPosition());
    m_shader->setUniformValue("cameraOffset", m_camera.getOffset());
    m_shader->setUniformValue("worldSize", QVector2D(WORLD_SIZE, WORLD_SIZE));
    m_shader->setUniformValue("verticalScale", m_verticalScale);
    m_shader->setUniformValue("horizontalScale", m_horizontalScale);
    m_shader->setUniformValue("lightPos", m_camera.moveLight(m_lightSource.position));
    m_shader->setUniformValue("lightIntensity", m_lightSource.intensity);
    m_shader->setUniformValue("lightColor", m_lightSource.color);
    m_shader->setUniformValue("textureCount", static_cast<GLint>(m_textures.size()));

    if (m_heightMap != nullptr)
    {
        m_shader->setUniformValue("heightMapSize", QVector2D(m_heightMap->width(), m_heightMap->height()));
        m_shader->setUniformValue("heightMapMaxValue", m_heightMap->max());
    }
    else
    {
        m_shader->setUniformValue("heightMapSize", QVector2D(1, 1));
        m_shader->setUniformValue("heightMapMaxValue", 0);
    }

    glBindVertexArray(m_heightMapVao);
    glPolygonMode(GL_FRONT_AND_BACK, m_showWireframe ? GL_LINE : GL_FILL);
    glPatchParameteri(GL_PATCH_VERTICES, 4);
    glDrawElements(GL_PATCHES, static_cast<int>(4 * m_world.numPatches), GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);

    measureTime(1);

    if (m_treeLoaded)
    {
        m_computeShader->bind();
        runComputeShader();
    }

    measureTime(2);

    if (m_treeLoaded) {
        m_treeShader->bind();
        m_treeShader->setUniformValue("v", m_camera.getViewMatrix());
        m_treeShader->setUniformValue("p", m_p);
        m_treeShader->setUniformValue("cameraPos", m_camera.getPosition());
        m_treeShader->setUniformValue("cameraOffset", m_camera.getOffset());
        m_treeShader->setUniformValue("worldSize", QVector2D(WORLD_SIZE, WORLD_SIZE));
        m_treeShader->setUniformValue("verticalScale", m_verticalScale);
        m_treeShader->setUniformValue("horizontalScale", m_horizontalScale);
        m_treeShader->setUniformValue("lightPos", m_camera.moveLight(m_lightSource.position));
        m_treeShader->setUniformValue("lightIntensity", m_lightSource.intensity);
        m_treeShader->setUniformValue("lightColor", m_lightSource.color);
        m_tree->draw(m_treeShader, m_camera.getModelMatrix(), m_drawCommandBuffer, m_treeGeometryBuffer, 100);
    }

    measureTime(3);

    if (m_treeLoaded) {
        m_impostorShader->bind();
        m_impostorShader->setUniformValue("m", m_camera.getModelMatrix());
        m_impostorShader->setUniformValue("v", m_camera.getViewMatrix());
        m_impostorShader->setUniformValue("p", m_p);
        m_impostorShader->setUniformValue("cameraPos", m_camera.getPosition());
        m_impostorShader->setUniformValue("angles", m_rots);
        m_tree->drawImpostor(m_drawCommandBuffer, m_treeImpostorBuffer);
    }

    measureTime(4);
#endif
    measureTime(5);
    reportTimings();
    m_evenFrame = !m_evenFrame;
}

void HelloGLWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    m_p.setToIdentity();
    m_p.perspective(45.0F, GLfloat(width) / height, 0.1F, 1000.0F);
    m_updateHeightMap = true;
}

void HelloGLWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_lastMousePos = event->pos();
        m_mousePressed = true;
    }
}

void HelloGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        m_mousePressed = false;
    }
}

void HelloGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (m_mousePressed)
    {
        m_camera.rotate(QVector2D(m_lastMousePos), QVector2D(event->pos()));
        m_lastMousePos = event->pos();
    }
}

void HelloGLWidget::updateHeightMap()
{
    if (m_heightMap == nullptr)
        return;

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_heightMapTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    for (unsigned int i = 0; i < m_heightMap->mipmaps()->size(); i++)
    {
        MipMap &mipmap = m_heightMap->mipmaps()->at(i);
        glTexImage2D(GL_TEXTURE_2D, static_cast<int>(i), GL_R32I, mipmap.width, mipmap.height, 0, GL_RED_INTEGER, GL_UNSIGNED_SHORT, mipmap.data->data());
    }
}

void HelloGLWidget::loadTextures()
{
    m_textures.push_back(QImage(":/img/facture_rock_0.jpg"));
    createMaterial(QVector3D(0.59F, 0.22F, 0), 3);

    m_textures.push_back(QImage(":/img/facture_rock_1.jpg"));
    createMaterial(QVector3D(0.67F, 0.32F, 0.11F), 3);

    m_textures.push_back(QImage(":/img/facture_sand.jpg"));
    createMaterial(QVector3D(0.77F, 0.7F, 0.55F), 1);

    m_textures.push_back(QImage(":/img/facture_stone.jpg"));
    createMaterial(QVector3D(0.46F, 0.45F, 0.41F), 6);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D_ARRAY, m_arrayTexture);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    int minWidth = INT_MAX;
    int minHeight = INT_MAX;
    for (size_t i = 0; i < m_textures.size(); i++)
    {
        QImage &texture = m_textures[i];
        if (texture.width() < minWidth)
        {
            minWidth = texture.width();
        }
        if (texture.height() < minHeight)
        {
            minHeight = texture.height();
        }
    }

    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, minWidth, minHeight, static_cast<int>(m_textures.size()));

    for (size_t i = 0; i < m_textures.size(); i++)
    {
        QImage &texture = m_textures[i];
        if (texture.width() != minWidth || texture.height() != minHeight)
        {
            texture = texture.scaled(minWidth, minHeight);
        }

        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, static_cast<int>(i), minWidth, minHeight, 1, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, texture.bits());
    }
}

void HelloGLWidget::createMaterial(QVector3D specularFactor, float shininessFactor)
{
    Material mat;
    mat.specularFactor[0] = specularFactor.x();
    mat.specularFactor[1] = specularFactor.y();
    mat.specularFactor[2] = specularFactor.z();
    mat.shininessFactor = shininessFactor;
    m_materials.push_back(mat);
}

void HelloGLWidget::initComputeShader()
{
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_tree->getPositionBuffer());

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_drawCommandBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 5 * 5 * sizeof(GLuint), nullptr, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_drawCommandBuffer);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_treeIndexBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 3 * sizeof(GLuint), nullptr, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, m_treeIndexBuffer);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_treeGeometryBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 4 * m_tree->getTreeCount() * sizeof(GLfloat), nullptr, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, m_treeGeometryBuffer);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_treeImpostorBuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 4 * m_tree->getTreeCount() * sizeof(GLfloat), nullptr, GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, m_treeImpostorBuffer);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    std::vector<GLuint> vertexCounts = m_tree->getVertexCounts();
    m_computeShader->setUniformValue("vertexCounts", QVector4D(vertexCounts[0], vertexCounts[1], vertexCounts[2], vertexCounts[3]));
}

void HelloGLWidget::runComputeShader()
{
    std::vector<QVector4D> viewFrustum = m_camera.getViewFrustum(m_p);
    m_computeShader->setUniformValue("frustumR", viewFrustum[0]);
    m_computeShader->setUniformValue("frustumL", viewFrustum[1]);
    m_computeShader->setUniformValue("frustumT", viewFrustum[2]);
    m_computeShader->setUniformValue("frustumB", viewFrustum[3]);
    m_computeShader->setUniformValue("frustumN", viewFrustum[4]);
    m_computeShader->setUniformValue("frustumF", viewFrustum[5]);
    m_computeShader->setUniformValue("bSphere", m_tree->getBoundingSphere());
    m_computeShader->setUniformValue("m", m_camera.getModelMatrix());
    m_computeShader->setUniformValue("cameraPos", m_camera.getPosition());
    m_computeShader->setUniformValue("numberOfTrees", m_numberOfTrees);
    m_computeShader->setUniformValue("geometryDistance", m_geomDist);
    m_computeShader->setUniformValue("impostorDistance", m_impDist);

    GLint workGroupSize[3];
    glGetProgramiv(m_computeShader->programId(), GL_COMPUTE_WORK_GROUP_SIZE, workGroupSize);
    glDispatchCompute(static_cast<GLuint>(m_numberOfTrees / workGroupSize[0] + 1), 1, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

#if 0
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_tree->getPositionBuffer());
    QVector4D* data0 = (QVector4D*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 100 * sizeof(QVector4D), GL_MAP_READ_BIT);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_treeImpostorBuffer);
    QVector4D* data1 = (QVector4D*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 100 * sizeof(QVector4D), GL_MAP_READ_BIT);
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    qDebug() << "IN" << data0[0];
    qDebug() << "OUT" << data1[0];
#endif
}

void HelloGLWidget::measureTime(unsigned int index)
{
    std::pair<GLuint, GLuint> &pair = m_queries[index];
    glQueryCounter(m_evenFrame ? pair.first : pair.second, GL_TIMESTAMP);
    GLuint64 result;
    glGetQueryObjectui64v(m_evenFrame ? pair.second : pair.first, GL_QUERY_RESULT, &result);
    m_queryResults.push_back(result);
}

void HelloGLWidget::reportTimings()
{
    std::vector<GLuint64> timings;
    if (m_queryResults.size() < 2)
        return;

    for (unsigned int i = 1; i < m_queryResults.size(); i++)
    {
        GLuint64 t0 = m_queryResults.at(i - 1);
        GLuint64 t1 = m_queryResults.at(i);
        timings.push_back(t1 - t0);
    }

    m_timingGraph->pushData(m_frameNumber++, timings);
}

float HelloGLWidget::map(int i, float start1, float end1, float start2, float end2)
{
    return (i - start1) / (end1 - start1) * (end2 - start2) + start2;
}
