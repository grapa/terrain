#version 440

layout(quads, equal_spacing, ccw) in;

in vec3 pos_TE[];
in vec2 uvHeightMap_TE[];
in vec2 uvTexture_TE[];
in float mipmapLevel_TE[];

out vec3 pos_FS;
out vec3 normal_FS;
out vec2 uv_FS;
out float height_FS;

uniform mat4 v;
uniform mat4 p;
uniform float verticalScale;
uniform vec2 heightMapSize;
uniform float horizontalScale;

layout(binding = 0) uniform isampler2D heightMap;

float map(float i, float start1, float end1, float start2, float end2)
{
    return (i - start1) / (end1 - start1) * (end2 - start2) + start2;
}

#define INTERPOLATE(type) \
    type interpolate(type a, type b, type c, type d) \
    { \
        type x = mix(a, b, gl_TessCoord.x); \
        type y = mix(c, d, gl_TessCoord.x); \
        return mix(x, y, gl_TessCoord.y); \
    }

INTERPOLATE(vec3)
INTERPOLATE(vec2)
INTERPOLATE(float)

void main(void)
{
    pos_FS = interpolate(pos_TE[0], pos_TE[1], pos_TE[2], pos_TE[3]);
    uv_FS = interpolate(uvTexture_TE[0], uvTexture_TE[1], uvTexture_TE[2], uvTexture_TE[3]);

    vec2 uv = interpolate(uvHeightMap_TE[0], uvHeightMap_TE[1], uvHeightMap_TE[2], uvHeightMap_TE[3]);
    int mipmapLevel = int(interpolate(mipmapLevel_TE[0], mipmapLevel_TE[3], mipmapLevel_TE[1], mipmapLevel_TE[2]));
    if (gl_TessCoord.x == 0 || gl_TessCoord.x == 1 || gl_TessCoord.y == 0 || gl_TessCoord.y == 1)
    {
        mipmapLevel = 0;
    }

    float texSize = textureSize(heightMap, mipmapLevel).x;
    ivec2 pixel = ivec2(uv * texSize);
    height_FS = texelFetch(heightMap, pixel, mipmapLevel).r;
    gl_Position = (p * v) * vec4(pos_FS.x, height_FS * verticalScale, pos_FS.z, 1);

    vec3 p0 = vec3(0);
    vec3 p1 = vec3(0);
    vec3 p2 = vec3(0);

    p0.y = texelFetch(heightMap, pixel, mipmapLevel).r;
    p1.y = texelFetch(heightMap, pixel + ivec2(1, 0), mipmapLevel).r;
    p2.y = texelFetch(heightMap, pixel + ivec2(0, -1), mipmapLevel).r;

    float scale = heightMapSize.x / texSize / horizontalScale;

    p1.x = scale;
    p2.z = scale;

    vec3 hx = p1 - p0;
    vec3 hz = p2 - p0;

    hx.y *= verticalScale;
    hz.y *= verticalScale;

    normal_FS = normalize(cross(hz, hx));
}
