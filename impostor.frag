#version 440

in vec2 uv_FS;
flat in int i_FS;

out vec4 frag;

layout(binding = 5) uniform sampler2DArray textureArray;

void main(void)
{
    vec4 color = texture(textureArray, vec3(uv_FS, i_FS));
    if (color.a == 0)
        discard;

    frag = color;
    // frag = vec4(1, i_FS / 36.0, 1, 1);
}
