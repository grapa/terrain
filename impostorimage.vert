#version 440

#define PI 3.14159

layout(location = 0) in vec3 pos_VS;
layout(location = 2) in vec2 uv_VS;

out vec2 uv_FS;

uniform float angle;

mat4 rotMat(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1 - c;

    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
                oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
                oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
                0.0, 0.0, 0.0, 1.0);
}

void main(void)
{
    vec3 pos = pos_VS;
    pos = vec3(rotMat(vec3(1, 0, 0), PI / 2) * vec4(pos, 1));
    pos = vec3(rotMat(vec3(0, 1, 0), angle) * vec4(pos, 1));
    pos -= vec3(0.6, 1, 0);

    gl_Position = vec4(pos, 1);
    uv_FS = uv_VS;
}
