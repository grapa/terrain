#ifndef WORLDGENERATOR_H
#define WORLDGENERATOR_H

#include <QOpenGLFunctions_4_4_Core>

struct World
{
    unsigned int numVertices;
    unsigned int numPatches;
    GLfloat* vertices;
    GLfloat* textureCoords;
    GLuint* indices;
};

class WorldGenerator
{
public:
    WorldGenerator();

    World generateWorld(unsigned int size);
};

#endif // WORLDGENERATOR_H
