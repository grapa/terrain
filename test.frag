#version 440

in vec2 uv_FS;

out vec4 frag;

layout(binding = 5) uniform sampler2DArray textureArray;

void main(void)
{
    vec4 color = texture(textureArray, vec3(uv_FS, 0));
    if (color.a == 0)
        discard;

    frag = color;
}
